# Kaizen Scheduler SSH Router

A router for Kaizen that executes jobs on a given host over SSH. Also includes basic log collection to local disk.

Run with:

```
runner-manage runserver 127.0.0.1:8000
```
