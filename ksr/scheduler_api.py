import os
import logging
import resource
from django.conf import settings
from django.utils import timezone
from django.core.urlresolvers import reverse
from rest_framework.renderers import JSONRenderer
from ksr.jobs.models import RouterJobMsg

log = logging.getLogger(__name__)


def notify_running(job, discovery=None):
    msg = {
        'jid': job.jid,
        'pid': job.pid,
        'router_link': settings.BASE_URL + reverse('jobs:detail', args=[job.id]),
        'discovery': discovery or {}
    }

    _send_message("v1/instances/{jid}/running/", job, msg)
    # TODO: shouldn't this just be UTC?
    job.running_notified = timezone.now()
    job.save()


def notify_succeeded(job):
    msg = {
        'jid': job.jid,
        'exit_code': job.exit_code
    }

    _send_message("v1/instances/{jid}/succeeded/", job, msg)
    job.exit_notified = timezone.now()
    job.save()


def notify_failed(job):
    # Collect comprehensive exit info
    status = job.exit_code

    exit_info = {
        'COREDUMP': os.WCOREDUMP(status),
        'IFCONTINUED': os.WIFCONTINUED(status),
        'IFSTOPPED': os.WIFSTOPPED(status),
        'IFSIGNALED': os.WIFSIGNALED(status),
        'IFEXITED': os.WIFEXITED(status),
        'EXITSTATUS': os.WEXITSTATUS(status),
        'STOPSIG': os.WSTOPSIG(status),
        'TERMSIG': os.WTERMSIG(status)
    }
    log.debug("Exit info: %s", exit_info)

    # Collect resource usage information
    resource_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    log.debug("Resource info: %s", resource_info)

    # TODO: what should this message actually look like
    msg = {
        'jid': job.jid,
        'exit_info': exit_info
    }

    _send_message("v1/instances/{jid}/failed/", job, msg)
    job.exit_notified = timezone.now()
    job.save()


def _send_message(endpoint, job, msg):
    """ Send a message to the job callback URL """
    if job.callback_url == "MOCK":
        # Useful just for testing, probably shouldn't stay here
        return

    url = os.path.join(job.callback_url, endpoint).format(jid=job.jid)
    data = JSONRenderer().render(msg)

    # Create a record of the message
    msg = RouterJobMsg.objects.create(job=job, url=url, msg=data)
    msg.send()
