import json
from rest_framework import serializers
from ksr.jobs.models import RouterJob


def check_required(field, value, full_field):
    if "." not in field:
        if not isinstance(value, dict) or field not in value:
            raise serializers.ValidationError("Missing key '%s'" % full_field)
    else:
        f, rest = field.split(".", 1)
        check_required(rest, value[f], full_field)


class JSONDictSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        if not isinstance(data, dict):
            raise serializers.ValidationError("Data %s is not a JSON dictionary", data)
        return json.dumps(data)

    def to_representation(self, value):
        repr = json.loads(value)
        if not isinstance(repr, dict):
            raise serializers.ValidationError("Value %s is not a JSON dictionary", value)
        return repr


class RouterJobSerializer(serializers.ModelSerializer):

    definition_required_fields = [
        'logging.collector.class',
        'logging.collector.url',
    ]

    definition = JSONDictSerializerField()

    def validate_definition(self, value):
        value = json.loads(value)
        [check_required(f, value, f) for f in self.definition_required_fields]
        return json.dumps(value)

    class Meta:
        model = RouterJob
        read_only_fields = [
            'exit_code', 'pid', 'start_time', 'end_time',
            'running_notified', 'exit_notified'
        ]
