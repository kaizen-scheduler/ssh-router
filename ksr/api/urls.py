from rest_framework import routers
import viewsets

router = routers.DefaultRouter()
router.register('jobs', viewsets.RouterJobViewSet)
urlpatterns = router.urls
