from django.shortcuts import get_object_or_404
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import viewsets
from ksr.jobs.models import RouterJob, RouterJobMsg
from serializers import RouterJobSerializer


class RouterJobViewSet(viewsets.ModelViewSet):
    queryset = RouterJob.objects.all()
    serializer_class = RouterJobSerializer

    @list_route(methods=['POST'])
    def refresh(self, request):
        """ Send any outstanding messages for the jobs we know about """
        for msg in RouterJobMsg.objects.filter(sent_at__isnull=True):
            msg.send()
        return Response()

    @detail_route(methods=['POST'])
    def kill(self, request, pk=None):
        """ Kill the given job.

        FIXME: this means a kill message needs to be sent to the router's
        job ID, not the scheduler's jid. Should the scheduler remember the ID
        and post back to that? Probably not. However if multiple schedulers use
        the same router then we need to avoid duplicate jids across schedulers.
        """
        job = get_object_or_404(RouterJob, id=pk)

        # FIXME: need to implement permissions based on token authentication that is
        # created on job launch. Each job should have its own token??

        # Since messages are idempotent, we should assume if we get a kill message
        # and the job is already dead then either the message is lost, or there is
        # a race. In any case, we should re-send the message. We only remove the
        # record of the job when the scheduler sends a DELETE message effectively
        # 'ack'ing the death of the router job.
        job.kill()

        return Response()
