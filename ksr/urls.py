from django.conf.urls import url, include
from django.http import HttpResponseRedirect

urlpatterns = [
    url(r'^api/', include('ksr.api.urls')),
    url(r'^jobs/', include('ksr.jobs.urls')),
    url(r'^$', lambda r: HttpResponseRedirect('jobs/')),
]
