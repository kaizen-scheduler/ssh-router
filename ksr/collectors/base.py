""" Base Logging Classes """
import io
import os
import threading


class BaseLogCollector(threading.Thread):

    def __init__(self, fd):
        self.fd = fd
        self._offset = 0
        super(BaseLogCollector, self).__init__()

    def handle(self, msg, offset):
        """ Handle a message from the FD """
        raise NotImplementedError()

    def close(self):
        """ Optional hook to close """
        pass

    def run(self):
        for msg in iter(lambda: os.read(self.fd, io.DEFAULT_BUFFER_SIZE), ''):
            self.handle(msg, self._offset)
            self._offset += len(msg)
        os.close(self.fd)
        self.close()
