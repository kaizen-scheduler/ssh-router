"""
For now, the clone collector writes to local disk
Eventually it should write to persistent store off-disk
as the scheduler won't always be running on the same
box!
"""

from file import FileLogCollector


class CloneListCollector(FileLogCollector):
    pass
