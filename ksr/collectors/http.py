import requests
from django.conf import settings
from base import BaseLogCollector
import logging


class HTTPLogCollector(BaseLogCollector):

    def __init__(self, fd, url=None):
        self.url = url
        super(HTTPLogCollector, self).__init__(fd)

    def handle(self, msg, offset):
        logging.info("Got msg: %s", msg)
        msg = {
            'data': msg
        }
        resp = requests.put(
            self.url, json=msg,
            auth=requests.auth.HTTPBasicAuth(settings.KAIZEN_USERNAME, settings.KAIZEN_PASSWORD)
        )
        resp.raise_for_status()
