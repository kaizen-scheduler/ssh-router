""" File-based log handler """
from base import BaseLogCollector


class FileLogCollector(BaseLogCollector):

    def __init__(self, fd, path, append=False):
        # Open our output file in the correct mode
        self._out = open(path, 'a+' if append else 'w+')
        # Set our initial offset, will be zero if append=False
        self._initial_offset = self._out.tell()
        super(FileLogCollector, self).__init__(fd)

    def handle(self, msg, offset):
        # Seek and write the message
        self._out.seek(self._initial_offset + offset)
        self._out.write(msg)

    def close(self):
        self._out.close()
