from django.views import generic
from ksr.jobs.models import RouterJob


class RouterJobList(generic.ListView):
    model = RouterJob
    paginate_by = 20


class RouterJobDetail(generic.DetailView):
    model = RouterJob
