from datetime import datetime
import json
import logging
import os
import subprocess
import signal
import sys

from django.conf import settings
from django.db import models
from django.dispatch import receiver
import requests

log = logging.getLogger(__name__)


class RouterJob(models.Model):

    # The Kaizen job instance ID
    jid = models.BigIntegerField()

    # The name given to the job
    name = models.CharField(max_length=256)

    # JSON field containing the definition
    definition = models.TextField()

    # Callback URL
    callback_url = models.CharField(max_length=256)

    # Current exit status of the process
    exit_code = models.IntegerField(blank=True, null=True)

    # Pid of the process
    pid = models.IntegerField(blank=True, null=True)

    # Start and End times of the process
    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True)

    # Notification Timestamps
    running_notified = models.DateTimeField(null=True)
    exit_notified = models.DateTimeField(null=True)

    @property
    def json_definition(self):
        return json.loads(self.definition)

    @property
    def is_running(self):
        return self.pid is not None and self.exit_code is None

    @property
    def is_finished(self):
        return self.pid is not None and self.exit_code is not None

    @property
    def is_launching(self):
        return self.pid is None

    @property
    def is_succeeded(self):
        return self.is_finished and self.exit_code == 0

    @property
    def is_failed(self):
        return self.is_finished and not self.is_succeeded

    def kill(self, kill_signal=signal.SIGKILL):
        if not self.is_finished:
            # Kill the entire process group since we changed it when
            # fork/exec'ing
            os.killpg(self.pid, kill_signal)

    class Meta:
        ordering = ['-exit_notified', '-running_notified']


class RouterJobMsg(models.Model):

    # The job this message relates to
    job = models.ForeignKey(RouterJob)

    # The url the message is to be sent to
    url = models.TextField()

    # The JSON message contents
    msg = models.TextField()

    # When the message was created
    created_at = models.DateTimeField(auto_now_add=True)

    # When the message was successfully sent
    sent_at = models.DateTimeField(blank=True, null=True)

    def send(self):
        """ Send this message """
        try:
            requests.post(
                self.url, data=self.msg, headers={'Content-Type': 'application/json'},
                auth=requests.auth.HTTPBasicAuth(settings.KAIZEN_USERNAME, settings.KAIZEN_PASSWORD)
            ).raise_for_status()
        except requests.HTTPError:
            log.exception("Failed to send router job message: %s", self)
            raise
        self.sent_at = datetime.utcnow()
        self.save()


@receiver(models.signals.post_save, sender=RouterJob)
def on_router_job_created(sender, instance=None, created=False, **kwargs):
    """ Launch a ksr-worker process to manage the job from now on.
    The worker survives for the duration of the job. However we want to
    respond to the API request now. So we spawn the worker process and detach
    it from ourselves by changing the session and group IDs. """
    if created:

        def detach():
            os.setsid()
            os.setpgrp()

        subprocess.Popen(
            [settings.KSR_WORKER, str(instance.id)],
            stdin=open(os.devnull, 'r'),
            stdout=sys.stdout,
            stderr=sys.stderr,
            preexec_fn=detach,
            close_fds=True,
            env=os.environ
        )
