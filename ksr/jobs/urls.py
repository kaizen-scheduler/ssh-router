from django.conf.urls import url
import views

app_name = 'jobs'
urlpatterns = [
    url(r'^$', views.RouterJobList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', views.RouterJobDetail.as_view(), name='detail'),
]
