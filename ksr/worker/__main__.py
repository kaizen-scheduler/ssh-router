import argparse
import logging
import os
import django
import django.core.management

logging.basicConfig()
log = logging.getLogger(__name__)


def add_arguments(parser):
    parser.description = "KSR worker process. DO NOT START MANUALLY."
    parser.add_argument(
        'router_job_id', type=int,
        help='The ID of the router job to launch'
    )


def main():
    """ Main method, parse the arguments and launch """
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()

    # Ensure DJANGO_SETTINGS_MODULE is configured and setup Django
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ksr.settings')
    django.setup()

    # Now Django is setup we can import things
    from ksr.jobs.models import RouterJob
    import launcher

    job = RouterJob.objects.get(id=args.router_job_id)

    if job.pid is not None:
        # FIXME: clear race here, perhaps we should create a lock file?
        raise Exception("Process is already run with pid %d. Aborting...", job.pid)

    # FIXME: we should send notify_error if launch fails for any reason
    launcher.launch(job)
