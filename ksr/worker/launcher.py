import logging
import os
import socket
import sys
from django.utils import timezone
from ksr import scheduler_api

log = logging.getLogger(__name__)


def launch(job):
    """ Launch a RouterJob """
    extra_env = {}

    # Configure Logging
    pipes, collectors = _configure_logging(job)

    # Configure any log handle environment variables
    for handle, (r, w) in pipes.iteritems():
        if not _handle_is_std(handle):
            var = "LOGGING_" + handle.upper()
            extra_env[var] = str(w)

    # Grab a free port
    # TODO: this isn't generic, what if someone needs more than one port?
    free_port = _get_free_port()
    extra_env['KSR_FREE_PORT'] = str(free_port)

    # Build up info for discovery
    discovery = {
        'hostname': _get_hostname(),
        'port': free_port
    }

    # Create a pipe to send a launch signal (by closing one end)
    lr, lw = os.pipe()

    pid = os.fork()
    if pid == 0:  # Child
        # Set out process group to be equal to the pid
        os.setpgrp()

        # Close read ends of the pipes
        for handle, (r, w) in pipes.iteritems():
            os.close(r)

        (_r, stdout) = pipes['stdout']
        (_r, stderr) = pipes['stderr']

        # Wait for the parent to tell us to exec
        os.close(lw)
        os.close(lr)

        # Exec the job
        _exec_bash_job(job.json_definition, stdout, stderr, extra_env)
    else:  # Parent
        # Close read ends of the pipes
        for handle, (r, w) in pipes.iteritems():
            os.close(w)

        # Set the pid
        job.pid = pid
        job.start_time = timezone.now()
        job.save()

        # Start log collectors
        for handle, collector in collectors.iteritems():
            collector.start()

        # Notify we've launched
        # FIXME: if this fails, we should still go on to collect and save the exit code
        scheduler_api.notify_running(job, discovery=discovery)

        # Signal to launch the job
        os.close(lr)
        os.close(lw)

        # TODO: Periodically collect resource usage and exit code using os.wait4?
        (_pid, status) = os.waitpid(pid, 0)

        job.end_time = timezone.now()
        job.exit_code = status
        job.save()

        # Wait for collectors to finish
        for handle, collector in collectors.iteritems():
            collector.join()

        if job.exit_code == 0:
            scheduler_api.notify_succeeded(job)
        else:
            scheduler_api.notify_failed(job)


def _handle_is_std(handle):
    return handle == "stdout" or handle == "stderr"


def _configure_logging(job):
    """ Configure the logging assuming we're using the HTTP logging backend """
    log_def = job.json_definition.get('logging', {})
    collector_def = log_def['collector']

    path, cls = collector_def.pop('class').rsplit('.', 1)
    mod = __import__(path, fromlist=[cls])
    collector = getattr(mod, cls)

    collectors = {}
    handles = {}
    for handle in log_def['handles']:
        r, w = os.pipe()
        handles[handle] = r, w
        collectors[handle] = collector(r, **collector_def)

    log.info("Created log collectors: %s", collectors)

    return handles, collectors


def _exec_bash_job(proc_def, stdout, stderr, extra_env):
    """ Exec a bash job and return its exit code """

    # For now, default stdin to devnull
    stdin = os.open(os.devnull, os.O_RDONLY)
    stdout = stdout or os.open(os.devnull, os.O_WRONLY)
    stderr = stderr or os.open(os.devnull, os.O_WRONLY)

    os.dup2(stdin, sys.stdin.fileno())
    os.dup2(stdout, sys.stdout.fileno())
    os.dup2(stderr, sys.stderr.fileno())

    cmd = proc_def['command']
    env = proc_def.get('env', {})

    # Fold in extra environment variables
    env.update(extra_env)

    # Set the environment and execute
    os.environ.update(env)
    os.execvp('/bin/bash', ['/bin/bash', '-c', cmd])


def _get_free_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("", 0))
    s.listen(1)
    port = s.getsockname()[1]
    s.close()
    return port


def _get_hostname():
    return socket.gethostname()
